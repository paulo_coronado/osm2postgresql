#Import geojson to postgis  

import json
import psycopg2
import geojson

# Open the file
with open('export.geojson') as f:
    gj = geojson.load(f)
    
    #Open config.json
    with open('db_config.json') as config_file:
        config = json.load(config_file)
        
        #Create connection string from config.json
        conn_string = "dbname='{}' user='{}' password='{}'".format(config['dbname'], config['user'], config['password'])

        # Connect to the database
        conn = psycopg2.connect(conn_string)
        cur = conn.cursor()

        # Insert the features
        for feature in gj['features']:
            cur.execute("INSERT INTO museum (name, geom) VALUES (%s, ST_GeomFromGeoJSON(%s))", (feature['properties']['name'], json.dumps(feature['geometry'])))
        conn.commit()       

        # Close the connection
        cur.close()
        conn.close()




    
    

    





